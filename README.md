# ModularPathfindingTrajectoryGeneration

#Gitlab Repo for Matthew Shelley MSc Dissertation

#Generation framework methods stored within 'GenerationMethodsFinal' Notebook

#Analysis methods stored in 'TrajectoryAnalysis' Notebook

#Generated Trajectories with no concept drift stored in 'generated_trajectories_knutsfordairportcorrected.csv'

#Generated Trajectories with concept drift stored in 'generated_trajectories_conceptdrift.csv'
